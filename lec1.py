import tellurium as te

r = te.loada("""

    sumad := A + D  # := happens continually
    
    A -> B; k1*A
    B -> C; k2*B
    B + C -> D; k3*B*C
    
    A = 10;  # mM
    k1 = 0.5;  # sec
    k2 = 0.23  # sec
    k3 = 0.24  # sec
""")

m = r.simulate(0, 20, 1000)  # first 2 digits indicate simulation time. Last digit gives number of points (resolution)
m = r.simulate(0, 20, ['time', 'sumad'])

# print(m)
# r.plot()  # Can insert figsize = (x, y)
#
# print(m.shape)

t = te.loada("""

    J1: -> mRNA; k1*A;
    J2: mRNA ->; k2*mRNA;
    J3: -> P1; k3*mRNA;
     P1 ->; k4*P1;
    
    A = 10;
    k1 = 0.5
    k2 = 0.24
    k3 = 0.67
    k4 = 0.45
""")
b = t.simulate(0, 20)
t.plot()

