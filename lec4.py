import tellurium as te

r = te.loada("""

    J1: -> Iint; a * (Iext - Iint) * P
    J2: -> P; c * Iint
    
    # degradation:
    Iint -> ; b * Iint
    P -> ; e * P


    a = 0.5
    b = 0.03
    c = 0.8
    e = 0.9
    Iext = 1
    Iint = 0.1
    P = 0.1
""")

m = r.simulate(0, 100)
r.plot()

# r = te.loada("""
#
#     J1: -> A; vin
#     J2: A -> B; k1*A
#     J3: B -> A; k2*B
#     J4: B -> ; k3*B
#
#
#     A = 40
#     B = 60
#     k1 = 0.1
#     k2 = 0.2
#     k3 = 0.1
#     vin = 1
# """)
# m = r.simulate()
# r.plot()