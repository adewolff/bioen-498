import numpy as np
from random import uniform
from random import randint
import tellurium as te
import matplotlib.pyplot as plt
import math

# 1
array = np.zeros(shape=(5, 2))
dim = np.shape(array)
num_elements = dim[0]

d = np.arange(0, 5, 0.5)

a = []
b = []
for x in d:
    a.append(round(x, 2))
    b.append(round(x*x, 2))
text = np.array([a, b])

np.savetxt('/home/alex/Documents/school/bioen498/python_project/test.txt', text)

text2 = np.loadtxt('/home/alex/Documents/school/bioen498/python_project/test.txt')
print(text2)

# 5
random_dist = np.random.random(100)
rand_array_uni = np.random.uniform(size=(10, 10))
rand_array_norm = np.random.normal(size=(10, 10))

# 8

r1 = te.loada("""
    J1: -> P1; k*I
    J2: -> P2; k*P1

    # Degradation of proteins
    P1 -> ; k*P1
    P2 -> ; k*P2

    # Setting constants
    k = 2
    I = 5
""")

r2 = te.loada("""
    J1: -> P1; k*I
    J2: -> P2; k*P1

    # Degradation of proteins
    P1 -> ; k*P1
    P2 -> ; k*P2

    # Setting constants
    k = 2
    I = 5
""")

m1 = r1.simulate(0, 20, 1000)
m2 = r2.simulate(0, 20, 1000)

m3 = np.vstack((m1, m2))
te.plotArray(m3)

# Plotting exercises

# 1
x = list(range(1, 11))
y = list()
for t in range(10):
    y.append(uniform(1, 100))

plt.plot(x, y)
plt.show()

# 2
sin = list()
for x in range(1, 101):
    sin.append(math.sin(x))
cos = list()
for x in range(1, 101):
    cos.append(math.cos(x))

plt.plot(sin, cos, color='orange', linewidth=2)
plt.xlabel("this is x")
plt.ylabel("this is y")
plt.title("this is a title")
plt.legend("s")
plt.show()

# 7
plt.subplot(2, 1, 1)
plt.plot(sin)

plt.subplot(2, 1, 2)
plt.plot(cos)

plt.show()

# 8
rand_array_norm2 = np.random.normal(loc=0, scale=1, size=(10, 10))
plt.hist(rand_array_norm2)
plt.show()

# Strings, for loops and conditionals
# 1
num_nucleo = randint(1, 60)
while num_nucleo % 3 != 0:
    num_nucleo = randint(1, 60)

amino_acids = ("A", "T", "C", "G")
sequence = list()
for x in range(num_nucleo):
    ind = randint(0, 3)
    amino = amino_acids[ind]
    sequence.append(amino)

# 2
num_bases = len(sequence) / 3

# Other exercises
# 1

while True:
    molar_concen = input("enter molar concentration of hydrogen ions: ")
    try:
        int(molar_concen)
        break
    except:
        print("that's not a number")
        continue

ph = 1 - math.log(10, int(molar_concen))
print("pH = {}".format(round(ph, 2)))

# 2
alist = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
list_less_five = list()

for x in range(0, len(alist)):
    a = alist[x]
    if a < 5:
        list_less_five.append(a)

print(list_less_five)

# 3
alist2 = [1, 4, 8, 3, 6]
