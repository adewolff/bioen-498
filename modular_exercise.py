import tellurium as te

model = te.loada("""
    model complex()
        B -> + S;
        S -> + B;
        
        S -> + M;
        M -> + S;
        
        M -> + L;
        L -> + M;
        
        B + glu -> + Bn;
        Bn -> + B + glu;
        
        S + glu -> + Sn;
        Sn -> + S + glu;
        
        L + glu -> + Ln;
        Ln -> + L + glu;
    end
    
    0: complex();
    1: complex();
    2: complex();
    3: complex();
    4: complex();
    
    1.B := 0.Bn
    1.S := 0.Sn
    1.L := 0.Ln
    
    2.B := 1.Bn
    2.S := 1.Sn
    2.L := 1.Ln
    
    3.B := 2.Bn
    3.S := 2.Sn
    3.L := 2.Ln
    
    4.B := 3.Bn
    4.S := 3.Sn
    4.L := 3.Ln

""")

res = model.simulate()
model.plot()
