import lmfit
import pylab as plt
import tellurium as te


def modelSimulate(v0, ka, kb, kc):
    model = te.loada("""
        
        J1: -> A; v0
        A -> B; ka*A
        B -> C; kb*B
        C -> ; kc*C
        
        # Constants:
        v0 = 10
        ka = 0.4
        kb = 0.32
        kc = ka
    """)
    model.v0 = v0
    model.ka = ka
    model.kb = kb
    model.kc = kc

    res = model.simulate()
    return (res)


# model_res = model.simulate()
#
# model_params = model.getGlobalParameterIds()
#
# exp_data = model_res['[B]']
#
# # Add noise
# for i in range(len(exp_data)):
#     exp_data[i] += random.random()

# Set up the parameters to be fit
params = lmfit.Parameters()
for p in model_params:
    current_value = model.model[p]
    params.add(p, value=current_value)
print(params)

experimental_outputs = ['B']
model_outputs = ['[B]']


def simulate(v0=10, ka=0.4, kb=0.32, kc=0.4):
    results = modelSimulate(v0, ka, kb, kc)
    return results['[B]']


# Other way:
model = lmfit.Model(simulate, independent_vars=[], method='leastsq')
fitter = model.fit(data, params)
fitter.params

# # Create the fitter
# minimizer = lmfit.Minimizer(residuals, params, fcn_args=(model, exp_data, experimental_outputs, model_outputs))
# result = minimizer.minimize()
# print(result.message)
#
# # Print fitting stats
# print("Results:")
# lmfit.report_fit(result.params, min_correl=0.5)

# Run model with param values obtained from fitting
for p in model_params:
    model.model[p] = result.params[p].value
fitted_model_res = model.simulate(0, 1200, 61)

plt.plot(fitted_model_res['time'], fitted_model_res['[B]'])
plt.show()

# Other way


# cross-validation exercise

# model = te.loada("""
#
#     J1: -> A; v0
#     A -> B; ka*A
#     B -> C; kb*B
#     C -> ; kc*C
#
#     # Constants:
#     v0 = 10
#     ka = 0.4
#     kb = 0.32
#     kc = ka
# """)
#
# res = model.simulate()
#
# b_val = res['[B]']
# time = res['time']
#
# generator = foldGenerator(len(b_val), 3)
#
# train_data = []
# test_data = []
#
# for train, test in generator:
#     train_data.append(train)
#     test_data.append(test)
#
# regress_results = []
# for i in range(len(train_data)):
#     regress_results.append(regress(time, b_val, train_data[i], test_data[i], order=1))
