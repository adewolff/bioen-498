import tellurium as te

r = te.loada("""
    J1: $A -> B; k1*A; # $ denotes a constant (boundary species, as opposed to floating species (B))
    B -> $C; k2*B;
    
    A = 10;
    k1 = 0.1;
    k2 = 0.3;
""")
# m = r.simulate(0, 50, 100, ['time', 'J1'])
m = r.simulate(0, 50, 100)
r.plot()