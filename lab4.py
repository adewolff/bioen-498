import lmfit
import numpy as np
import pylab as plt
import tellurium as te

r = te.loada("""
    
    J1: -> y1; p0
    J2: y1 -> ; y1;
    J3: y1 -> y2; p1*y1*(1+p4*y2^4)
    J4: y2 -> ; y2*p6;
    
    p0 = 7;
    p1 = 1;
    p4 = 1;
    p6 = 4.96;
    
""")

to_fit = ['y1', 'y2']

m = np.loadtxt('expdata.txt')

time_to_simulate = 10
n_data_points = np.shape(m)[0]

x_data = m[:, 0]
y_data = [m[:, 1], m[:, 2]]
species_list = ['[S1]', '[S2]']


def justResiduals(p, index, speciesId):
    return y_data[index] - my_ls_func(p, speciesId)


def my_ls_func(p, speciesId):
    r.reset()
    pp = p.valuesdict()
    for i in range(0, n_data_points):
        r.model[toFit[i]] = pp[toFit[i]]
    m = r.simulate(0, timeToSimulate, nDataPoints)
    return m[speciesId]


def residuals(p):
    r.reset()
    pp = p.valuesdict()
    for key in params.keys():
        r.model[key] = pp[key]
    m = r.simulate(0, time_to_simulate, n_data_points, ['y1', 'y2'])

    res = (y_data[0] - m['y1'])
    res = np.concatenate((res,))
    for k in range(1, len(['y1', 'y2'])):
        res = np.concatenate((res, (y_data[k] - m[:, k])))
    return res


to_fit = ['p0', 'p1', 'p4', 'p6']
params = lmfit.Parameters()
for p in to_fit:
    params.add(p, value=5, min=0, max=20)

minimizer = lmfit.Minimizer(residuals, params)
result = minimizer.minimize()
print("Results:")
lmfit.report_fit(result.params, min_correl=0.5)
ci = lmfit.conf_interval(minimizer, result)
lmfit.printfuncs.report_ci(ci)

for p in to_fit:
    r.model[p] = result.params[p].value
m = r.simulate(0, time_to_simulate, 10)

# plt.figure(figsize=(10, 8))
# plt.show()

# plot experimental data
for k in range(0, len(species_list)):
    plt.plot(x_data, y_data[k], 'dm', markersize=8)

# rerun model
r.reset()
m = r.simulate(0, 10, ['time', 'y1', 'y2'])

# plot fit lines
plt.plot(m['time'], m['y1'], '-b', linewidth=2)
plt.plot(m['time'], m['y2'], '-g', linewidth=2)
plt.show()

# # Plot the residuals for the indexth variable
# resids = justResiduals(result.params, 0, '[S1]')
# plt.plot(x_data, resids, 'bo', markersize=6)
# plt.vlines(x_data, [0], resids, color='r', linewidth=2)
#
# plt.tick_params(axis='both', which='major', labelsize=16)
# plt.xlabel('Time')
# plt.ylabel("Concentration", fontsize=16)
# plt.legend(['S1', 'S2', 'S3', 'Best fit', 'Best fit', 'Best fit', 'Residuals'], loc=0, fontsize=10)
# plt.axhline(y=0, color='k')
# # plt.savefig('fittedCurves.pdf')
# plt.show()
