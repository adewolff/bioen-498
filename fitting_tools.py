def residuals(parameters, model, experimental_results, exp_outputs, model_outputs):
    """
    :param parameters: lmfit.Parameters obj containing parameters and their values
    :param model: tellurium model obj containing model to be run
    :param experimental_results: NumPy array with experimental results to calculate residuals against
    :param exp_outputs:
    :param model_outputs:
    :return: NumPy array containing residuals
    """

    model.reset()
    param_vals = parameters.valuesdict()
    for p in parameters:
        model.model[p] = param_vals[p]
    res = model.simulate(0, 1200, 61)

    resids = []

    for i in range(len(exp_outputs)):
        exp_param = exp_outputs[i]
        res_param = model_outputs[i]
        resids.append(experimental_results[0] - res[res_param])

    return resids
