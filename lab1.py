import tellurium as te
import numpy as np

print(te.getVersionInfo())

a = np.array([2, 3, 4])
print(a)

b = np.array([[1, 2, 3], [4, 5, 6]])
print(b)
print(b[1])

# Initializing empty arrays;
c = np.zeros(shape=(5, 2))
print(c)

print("dimensions: ", c.ndim)
print("size: ", c.size)
print("shape: ", c.shape)

# Creating array with given sequence of numbers
d = np.arange(0, 10, 0.1)  # start, stop, interval


# Exercise

for x in np.arange(0, 5, 0.2):
    print(x, ',', x*x)

xl = []; yl = []
for x in np.arange(0, 5, 0.2):
    xl.append(x)
    yl.append(x * x)

example_list = np.array([xl, yl])
print(example_list)

np.dot(a * b)  # multiplies element by element like R
