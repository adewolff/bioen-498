import tellurium as te

model = te.loada("""

    E1: at(time > 10): $P1 = $P1 + 1
    E2: at(time > 18): $P1 = $P1 - 1

    # trigger:
    T1: -> P2; ($P1^n)/(K + $P1^n)
    T2: $P1 -> P3; (($P1^n)*(P2^n))/(K1 + K2*($P1^n) + K2 *(P2^n) + K3 *($P1^n) * (P2^n))
    TD1 -> ; P2*kd
    TD2 -> ; P3*kd
    
    # Latch:
    L1: -> A; Vm*(bas+((A^h)/((K^h)+(A^h))))*((K^h)/((K^h)+(B^h)))
    L2: -> B; Vm*(bas+((B^h)/((K^h)+(B^h))))*((K^h)/((K^h)+(A^h))) + P3
    LD1: A -> ; 1.0*A
    LD2: B -> ; 1.2*B
    
    # Amplifier:
    S1 -> S2; (2*B)*S1/(0.1 + S1)
    S2 -> S1; S2/(0.01 + S2);
    
    # Flux:
    Flux: $Xo-> M1; km1*S2*Xo
    M1 -> M2; km1*S2*M1
    M2 -> M3; km1*S2*M2
    M3 -> ;   km1*S2*M3
    

    # Constants:
    $P1 = 0.2
    n = 8
    K1 = 1
    K = 0.5
    K2 = 0.5
    K3 = 0.1
    kd = 0.1
    
    Vm = 0.7
    h = 4
    bas = 0.5
    
    S1 = 5
    S2 = 0
    
    km1 = 0.1
    Xo = 1;
    
""")

res = model.simulate(0, 100, 1000, ['time', '[B]', '[M1]', '[M2]', '[M3]'])
model.plot()

# for i in range(1, 8):
#     amplifier = te.loada("""
#
#         # Amplifier:
#         S1 -> S2; (2*B)*S1/(0.1 + S1)
#         S2 -> S1; S2/(0.01 + S2);
#
#         # Constants:
#         S1 = 5
#         S2 = 0
#         B = {}
#
#     """.format(i/10))
#     res_amp = amplifier.simulate(0, 50)
#
#     plt.plot(res_amp['time'], res_amp['[S1]'], label="S1 "+str(i/100))
#     plt.plot(res_amp['time'], res_amp['[S2]'], label="S2 "+str(i/100))
#
# plt.legend()
# plt.show()
