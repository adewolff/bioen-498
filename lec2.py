import tellurium as te

# r = te.loada("""
#     J1: ATP -> AMP + 2 _Pi; k*ATP
#
#     AMP = 0
#     _Pi = 0
#     ATP = 5
#     k=0.3
# """)
#
# # print(r.ATP)
# # print(r.J1)
#
# m = r.simulate(0, 30, 100)
# print(m)
# r.plot()


# """Class exercise"""
#
# r = te.loada("""
#     J1: -> P1; k*I
#     J2: -> P2; k*P1
#
#     # Degradation of proteins
#     P1 -> ; k*P1
#     P2 -> ; k*P2
#
#     # Setting constants
#     k = 2
#     I = 5
# """)
#
# m = r.simulate()
# r.plot()


r = te.loada("""
    
    # Gene 1
    translation1: -> mRNA1; k1*I
    transcription1: -> P1; k2*mRNA1
    
    # Gene 2molar_concen = input("enter molar concentration of hydrogen ions: ")
    translation2: -> mRNA2; k3*P1
    transcription2: -> P2; k4*mRNA2

    # Degradation of proteins
    P1 -> ; k5*P1
    P2 -> ; k6*P2
    
    # Degradation of mRNA
    mRNA1 -> ; k7*mRNA1
    mRNA2 -> ; k8*mRNA2

    # Setting constants
    k1 = 0.8
    k2 = 0.5
    k3 = 0.85
    k4 = 0.354
    k5 = 0.243
    k6 = 0.432
    k7 = 0.43292
    k8 = 0.53
    I = 5
""")
m = r.simulate ()
r.plot()

