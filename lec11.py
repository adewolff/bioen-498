import tellurium as te

model = te.loada("""

    J1: Xo -> GI; ko*Xo
    GI -> Plasma; k1*GI
    Plasma -> ; k2*Plasma
    Plasma -> Peripheral; k3*Plasma - k4*Peripheral
       
    at time > 10:
        Xo = 1
        
    ko = 0.1
    k1 = 0.23
    k2 = 0.5
    k3 = 0.45
    k4 = 0.05
    
""")
res = model.simulate(0, 100)
model.plot()
