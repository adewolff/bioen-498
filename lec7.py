import numpy as np
import tellurium as te
from sklearn import linear_model
from sklearn.metrics import r2_score

# Exercise: compare models using residuals
# model 1-B is experimental data

# r1 = te.loada("""
#
#     J1: -> A; v0
#     J2: A -> B; ka*A
#     J3: B -> C; kb*B
#
#
#     DEG1: C -> ; kc*C
#
#     v0 = 10
#     ka = 0.4
#     kb = 0.32
#     kc = ka
#
# """)
#
# m1 = r1.simulate()
#
# r2 = te.loada("""
#
#     J1: -> B; v0
#     J2: B ->; kb*B
#
#     v0 = 10
#     kb = 0.32
#
# """)
#
# m2 = r2.simulate()
#
# # Add noise:
# exp_data = [m1['time'], m1['[B]']]
#
# for i in range(len(exp_data)):
#     exp_data[i] = exp_data[i] + np.random.normal(0, 0.1)
#
#
# def residuals(p):
#     r2.reset()
#     pp = p.valuesdict()
#     for key in params.keys():
#         r2.model[key] = pp[key]
#     m = r2.simulate()
#
#     res = (exp_data[0] - m2['[B]'])
#     res = np.concatenate((res,))
#     return res
#
#
# params = lmfit.Parameters()
# params.add('B', value=5, min=0, max=20)
#
#
# minimizer = lmfit.Minimizer(residuals, params)
# result = minimizer.minimize()
# print("Results:")
# lmfit.report_fit(result.params, min_correl=0.5)
# ci = lmfit.conf_interval(minimizer, result)
# lmfit.printfuncs.report_ci(ci)
#
#
# r2.model['[B]'] = result.params['[B]'].value
# m = r2.simulate()


# Linear least squares regression

r1 = te.loada("""

    J1: -> A; v0
    J2: A -> B; ka*A
    J3: B -> C; kb*B


    DEG1: C -> ; kc*C

    v0 = 10
    ka = 0.4
    kb = 0.32
    kc = ka

""")

m1 = r1.simulate()

r2 = te.loada("""

    J1: -> B; v0
    J2: B ->; kb*B

    v0 = 10
    kb = 0.32   

""")

m2 = r2.simulate()

# Add noise:
training_set = [m1['[B]'] + np.random.normal(0, 0.1)]
test_set = [m1['[B]'] + np.random.normal(0, 0.1)]

# Create matrix
length = 20
std_dev = 1

# Construct vectors with 19 random values, and a value of 20
x_val = np.random.normal(0, std_dev, length - 1)
x_val = np.concatenate([x_val, np.array([length])])
y_val = np.random.normal(0, std_dev, length - 1)
y_val = np.concatenate([y_val, np.array([length])])

# Construct matrix
mat = np.matrix([np.repeat(1, length), x_val])
mat = mat.transpose()

# Fit, find constants evaluate
regr = linear_model.LinearRegression()
regr.fit(mat, y_val)

# Predicted values
y_preds = regr.predict(mat)

# R-squared
r_sqr = r2_score(y_val, y_preds)
