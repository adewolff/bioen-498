import numpy as np
import pylab
import tellurium as te

# and gate
model = te.loada("""

    J1: $s -> ; (x1^n/(0.5^n + x1^n)) * (x2^n/(0.5^n + x2^n))
    
    
    n = 8
    x1 = 0.6
    x2 = 0

""")

x2_range = np.arange(0, 1, 0.1)

for j in x2_range:
    x1_axis = []
    j1_axis = []
    x1_range = np.arange(0, 1, 0.02)
    model.x2 = j
    for i in x1_range:
        model.x1 = i
        x1_axis.append(i)
        j1_axis.append(model.J1)
    pylab.plot(x1_axis, j1_axis, label='k2{}'.format(j))

pylab.legend()
pylab.show()

# competitive or gate
# ;((x1 + x2)^n)/(0.5)^n + (x1)^n + (x2)^n

# noncompetitive or gate
# ;((x1)^n + (x2)^n)/(0.5)^n + (x1)^n + (x2)^n


# pulsing:
model = te.loada("""
    
    E1: at(sin(time)>0.25):A = A + 5
    E2: at(cos(time)>0.25):A = A - 5

    J1: $A -> ; k1*A
    J2: -> P1; k1*A
    
    k1 = 0.1
    A = 0

""")

res = model.simulate(0, 50, 100, ['time', 'A', 'P1'])
model.plot()
