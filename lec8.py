# Generate indices of train, test ata
import numpy as np
import tellurium as te
from sklearn import linear_model
from sklearn.metrics import r2_score


# CROSS VAL
def foldGenerator(num_points, num_folds):
    indices = range(num_points)
    for remainder in range(num_folds):
        test_indices = []
        for idx in indices:
            if idx % num_folds == remainder:
                test_indices.append(idx)
        train_indices = np.array(
            list(set(indices).difference(test_indices)))
        test_indices = np.array(test_indices)
        yield train_indices, test_indices


def buildMatrix(xv, order):
    """
    :param array-of-float xv:
    :return matrix:
    """
    length = len(xv)
    xv = xv.reshape(length)
    constants = np.repeat(1, length)
    constants = constants.reshape(length)
    data = [constants]
    for n in range(1, order + 1):
        data.append(xv * data[-1])
    mat = np.matrix(data)
    return mat.T


def regress(xv, yv, train, test, order=1):
    """
    :param array-of-float xv: predictor values
    :param array-of-float yv: response values
    :param array-of-int train: indices of training data
    :param array-of-int test: indices of test data
    :param int order: Order of the polynomial regression
    return float, array-float, array-float: R2, y_test, y_preds
    """
    regr = linear_model.LinearRegression()
    mat_train = buildMatrix(xv[train], order)
    regr.fit(mat_train, yv[train])
    mat_test = buildMatrix(xv[test], order)
    y_pred = regr.predict(mat_test)
    rsq = r2_score(yv[test], y_pred)
    # coeffi = linear_model.C
    return rsq, yv[test], y_pred


model = te.loada("""

    J1: -> A; v0
    J2: A -> B; ka*A
    J4: B -> C; kb*B
    J5: C -> ; kc*C
    
    # Constants
    v0 = 10
    ka = 0.4
    kb = 0.32
    kc = ka
""")

res = model.simulate()

b_val = res['[B]']
time = res['time']

generator = foldGenerator(len(b_val), 3)

train_data = []
test_data = []

for train, test in generator:
    train_data.append(train)
    test_data.append(test)

regress_results = []
for i in range(len(train_data)):
    regress_results.append(regress(time, b_val, train_data[i], test_data[i], order=1))


#     predictor = time


# BOOTSTRAP
def generateData(y_obs, y_fit):
    """
    :param np.array y_obs
    :param np.array y_fit
    :return np.array: bootstrap data
    """
    residuals = y_obs - y_fit
    length = len(y_obs)
    residuals = residuals.reshape(length)
    samples = np.random.randint(0, length, length)
    result = y_fit + residuals[samples]
    result = result.reshape(length)
    return result

# boot_data = generateData(b_val, regress_results[0][2])
# y_fit should be y_fit from generator
