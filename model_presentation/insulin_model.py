import pylab as plt
import tellurium as te

# Original model:
model = te.loadSBMLModel('BIOMD0000000372.xml')
result_orig = model.simulate(0, 480, ['time', 'Ip_conc', 'G_conc'])

# Plasma Insulin
plt.plot(result_orig['time'], result_orig['Ip_conc'])
plt.ylabel('Plasma Insulin (mU/I)')
plt.show()

# Plasma Glucose
plt.plot(result_orig['time'], result_orig['G_conc'])
plt.ylabel('Plasma Glucose (mg/dl)')
plt.show()

# model_te = te.sbmlToAntimony(model)
