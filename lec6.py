import lmfit
import numpy as np
import pylab as plt
import tellurium as te

r = te.loada("""

    A -> B; k1*A
    
    A = 5;
    k1 = 0.15

""")

time_to_simulate = 25
n_data_points = 5

m = r.simulate(0, time_to_simulate, n_data_points)

# Create independent data
x_data = m[:, 0]
y_data = m[:, 1]

# Add noise
for i in range(n_data_points):
    y_data[i] = y_data[i] + np.random.normal(0, 0.5)

# Plotting
plt.plot(x_data, y_data, marker='*', linestyle='None')
plt.show()


def residuals(p):
    r.reset()
    pp = p.valuesdict()
    r.k1 = pp['k1']
    m = r.simulate(0, time_to_simulate, n_data_points, ['A'])
    return y_data - m[:, 0]


# Set up parameters to be fit
parameters = lmfit.Parameters()
parameters.add('k1', value=1, min=0, max=10)

# Create optimizer
minimizer = lmfit.Minimizer(residuals, parameters)
result = minimizer.minimize()
print(result.message)
