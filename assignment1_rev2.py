import pylab as plt
import tellurium as te

a = te.loada("""

    J1: -> P; 1/(k1 + pow(P, h))

    # Protein degradation:
    Pdeg: P -> ; k2 * P


    # Constants:
    k1 = 0.1
    k2 = 0.05
    h = 9

""")

a_model = a.simulate(0, 1000)
a.plot(title='a')

b = te.loada("""

    J1: -> P1; 1/(k1 + pow(P2, h))
    J2: -> P2; k1 * P1

    #  Protein degradation:
    P1deg: P1 -> ; k2 * P1
    P2deg: P2 -> ; k2 * P2


    # Constants:
    k1 = 0.1
    k2 = 0.05
    h = 9

""")

b_model = b.simulate(0, 1000)
b.plot(title='b')

c = te.loada("""

    J1: -> P1; 1/(k1 + pow(P3, h))
    J2: -> P2; k1 * P1
    J3: -> P3; k1 * P2

    #  Protein degradation:
    P1deg: P1 -> ; k2 * P1
    P2deg: P2 -> ; k2 * P2
    P3deg: P3 -> ; k2 * P3


    # Constants:
    k1 = 0.1
    k2 = 0.05
    h = 9

""")

c_model = c.simulate(0, 1000)
c.plot(title='c')

d = te.loada("""

    J1: -> P1; 1/(k1 + pow(P4, h))
    J2: -> P2; k1 * P1
    J3: -> P3; k1 * P2
    J4: -> P4; k1 * P3

    #  Protein degradation:
    P1deg: P1 -> ; k2 * P1
    P2deg: P2 -> ; k2 * P2
    P3deg: P3 -> ; k2 * P3
    P4deg: P4 -> ; k2 * P4


    # Constants:
    k1 = 0.1
    k2 = 0.05
    h = 9

""")

d_model = d.simulate(0, 1000)
d.plot(title='d')

e = te.loada("""

    J1: -> P1; 1/(k1 + pow(P5, h))
    J2: -> P2; k1 * P1
    J3: -> P3; k1 * P2
    J4: -> P4; k1 * P3
    J5: -> P5; k1 * P4

    #  Protein degradation:
    P1deg: P1 -> ; k2 * P1
    P2deg: P2 -> ; k2 * P2
    P3deg: P3 -> ; k2 * P3
    P4deg: P4 -> ; k2 * P4
    P5deg: P5 -> ; k2 * P5


    # Constants:
    k1 = 0.1
    k2 = 0.05
    h = 9

""")

e_model = e.simulate(0, 1000)
e.plot(title='e')

f = te.loada("""

    J1: -> P1; 1/(k1 + pow(P10, h))
    J2: -> P2; k1 * P1
    J3: -> P3; k1 * P2
    J4: -> P4; k1 * P3
    J5: -> P5; k1 * P4
    J6: -> P6; k1 * P5
    J7: -> P7; k1 * P6
    J8: -> P8; k1 * P7
    J9: -> P9; k1 * P8
    J10: -> P10; k1 * P9

    #  Protein degradation:
    P1deg: P1 -> ; k2 * P1
    P2deg: P2 -> ; k2 * P2
    P3deg: P3 -> ; k2 * P3
    P4deg: P4 -> ; k2 * P4
    P5deg: P5 -> ; k2 * P5
    P6deg: P6 -> ; k2 * P6
    P7deg: P7 -> ; k2 * P7
    P8deg: P8 -> ; k2 * P8
    P9deg: P9 -> ; k2 * P9
    P10deg: P10 -> ; k2 * P10


    # Constants:
    k1 = 0.1
    k2 = 0.05
    h = 9

""")

f_model = f.simulate(0, 5000)
f.plot(title='f')


# Code for determining oscillatory behaviour:

# Obtains last reaction in the model
def lastReaction(mod):
    reactions = mod.getReactionIds()
    for i in reactions:
        if 'J' in i:
            last_reaction = i
    return last_reaction


# plots models with different values of hill coefficient
def oscilatron(mod):
    last_reaction = lastReaction(mod)
    for i in range(1, 11):
        mod.reset()
        mod.h = i
        m = mod.simulate(0, 5000, ['time', str(last_reaction)])
        plt.plot(m[:, 0], m[:, 1], label=i)
    plt.legend()
    plt.title("Hill coefficients at differing levels for {}".format(str(last_reaction)))
    plt.show()


models = [a, b, c, d, e, f]
model_names = ['a', 'b', 'c', 'd', 'e', 'f']

for i in models:
    oscilatron(i)
