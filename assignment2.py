import pylab as plt
import tellurium as te

model = te.loada("""
    
    MKKK -> MKKKP; v1*MKKK/((1+pow((MAPKPP/KI), n))*(K1+MKKK))
    MKKKP -> MKKK; v2*MKKKP/(K2+MKKKP)
    
    MKK -> MKKP; k3*MKKKP*MKK/(K3+MKK)
    MKKP -> MKKPP; k4*MKKKP*MKKP/(K4+MKKP)
    MKKPP -> MKKP; v5*MKKPP/(K5+MKKPP)
    MKKP -> MKK; v6*MKKP/(K6+MKKP)
    
    MAPK -> MAPKP; k7*MKKPP*MAPK/(K7+MAPK)
    MAPKP -> MAPKPP; k8*MKKPP*MAPKP/(K8+MAPKP)
    MAPKPP -> MAPKP; v9*MAPKPP/(K9+MAPKPP)
    MAPKP -> MAPK; v10*MAPKP/(K10+MAPKP)
        
    
    # params:
    v1 = 2.5; K1 = 10
    v2 = 0.25; K2 = 8
    k3 = 0.025; K3 = 15
    k4 = 0.025; K4 = 15
    v5 = 0.75; K5 = 15
    v6 = 0.75; K6 = 15
    k7 = 0.025; K7 = 15
    k8 = 0.025; K8 = 15
    v9 = 0.5; K9 = 15
    v10 = 0.5 ; K10 = 15
        
    KI = 9
    n = 1 
    
    # Init values:  
    MKKK = 100
    MKK = 300
    MAPK = 300 
""")

res = model.simulate(0, 12000)

# plot
plt.plot(res['time'], res['[MAPK]'], label='MAPK')
plt.plot(res['time'], res['[MAPKPP]'], label='MAPKPP')
plt.legend()
plt.title('graph A')
plt.show()

# Plot B
model_b = model

b_params = {'n': 2, 'KI': 18, 'K1': 50, 'K2': 40, 'v9': 1.25, 'v10': 1.25}
for i in range(3, 11):
    b_params['K' + str(i)] = 100

# Switch param values
for i in b_params:
    model_b.model[i] = b_params[i]

# plot
res_b = model_b.simulate(0, 12000)
plt.plot(res_b['time'], res_b['[MAPK]'], label='MAPK')
plt.plot(res_b['time'], res_b['[MAPKPP]'], label='MAPKPP')
plt.legend()
plt.title('graph B')
plt.show()
