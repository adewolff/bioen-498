import pylab as plt
import tellurium as te

r = te.loada("""
    
    $X0 -> A; (k1 * X0)
    A -> B; (kf * A)
    B -> A; (kr * B)
    B -> ; (k2 * B)
    
    
    # Constants:
    X0 = 1
    k1 = 0.1
    k2 = 0.5
    kf = 0.3
    kr = 0.4
    
""")

for i in range(1, 11):
    r.reset()
    r.X0 = i
    m = r.simulate()
    plt.plot(m[:, 0], m[:, 1])
plt.show()
