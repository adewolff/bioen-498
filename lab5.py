import pylab as plt
import tellurium as te

# 1
model = te.loada("""

    E1: at(time > 30): Xo = Xo + 1
    E2: at(time > 50): Xo = Xo - 1

    $Xo -> S1; k1*Xo
    S1 ->  ; k2*S1
    
    
    k1 = 0.1
    k2 = 0.2
    Xo = 1

""")

res = model.simulate(0, 100, 200, ['time', 'Xo', 'S1'])
model.plot()

# 2
model_c1 = te.loada("""

    E1: at(time > 10): $P1 = $P1 + 1
    E2: at(time > 18): $P1 = $P1 - 1
    
    # Reactions:
    J2: -> P2; ($P1^n)/(K + $P1^n)
    J3: $P1 -> P3; (($P1^n)*(P2^n))/(K1 + K2*($P1^n) + K2 *(P2^n) + K3 *($P1^n) * (P2^n))
    
    # degradation:
    P2 -> ; P2*kd
    P3 -> ; P3*kd
    
    # Constants:
    $P1 = 0.2
    n = 8
    K1 = 1
    K = 0.5
    K2 = 0.5
    K3 = 0.1
    kd = 0.1

""")

pulse_width = list(range(11, 25))
for i in pulse_width:
    model_c1 = te.loada("""

        E1: at(time > 10): $P1 = $P1 + 1
        E2: at(time > {}): $P1 = $P1 - 1

        # Reactions:
        J2: -> P2; ($P1^n)/(K + $P1^n)
        J3: $P1 -> P3; (($P1^n)*(P2^n))/(K1 + K2*($P1^n) + K2 *(P2^n) + K3 *($P1^n) * (P2^n))

        # degradation:
        P2 -> ; P2*kd
        P3 -> ; P3*kd

        # Constants:
        $P1 = 0.2
        n = 8
        K1 = 1
        K = 0.5
        K2 = 0.5
        K3 = 0.1
        kd = 0.1

    """.format(i))
    res = model_c1.simulate(0, 100, 200, ['time', 'P3'])
    plt.plot(res['time'], res['P3'], label='Pulse Width: ' + str(i - 10))

plt.legend()
plt.show()

# 4
model_l1 = te.loada("""

    E1: at(time > 50): $P1 = 1

    # Reactions:
    J2: -> P2; k1*($P1^n)/(K1 + P1^n)
    J3: $P1 -> P3; k2*((K2)/(K2 + P2^n)) * ((P1^n)/(K2+P1^n))
    
    # Degradation:
    P2 -> ; P2*kd2
    P3 -> ; P3*kd3
    
    # Constants:
    $P1 = 0
    n = 12
    K1 = 0.1
    K2 = 0.01
    
    k1 = 0.01
    k2 = 0.2
    
    kd2 = 0.01
    kd3 = 0.2    
    
""")

res_l1 = model_l1.simulate(0, 100, 200, ['time', 'P1', 'P3'])
model_l1.plot()
